---
title: 'О нас'
date: 2018-12-06T09:29:16+10:00
layout: 'about'
heroHeading: 'О нас'
heroSubHeading: "Начиная с 2011 года мы создавали команду лучших специалистов в регионе, в сфере проведения электромонтажных работ, подкрепленное опытом десятков специалистов со всей страны!"
heroBackground: 'images/about-hdr.jpg'
---

<div>
{{< content-strip-left "/pages/about" "content1" >}}
</div>
<div>
{{< content-strip-right "/pages/about" "content2" >}}
</div>
<div>
{{< content-strip-center "/pages/about" "content3" >}}
</div>
