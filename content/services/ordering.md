---
title: 'Заказ материалов'
date: 2018-11-28T15:14:54+10:00
icon: 'services/service-icon-5.png'
featured: true
draft: false
heroHeading: 'Заказ материалов'
heroSubHeading: 'Широкие возможности приобретения со скидкой'
heroBackground: 'services/service3.jpg'
---

Хотите получить коммерческое предложение на сборку щитов? [Пришлите нам](/contact) [вводную информацию](/services/installation) и мы вам ответим в кратчайшие сроки!

## Мы сотрудничаем со следующими партнерами по сборке электрощитового оборудования

- ![ABB](/services/abb.png)

- ![Legrand](/services/legrand.png)

- ![IEK](/services/iek.png)

- ![Sieens](/services/siemens.png)

- ![EKF](/services/ekf.png)

И можем предложить нашу скидку для своих клиентов!